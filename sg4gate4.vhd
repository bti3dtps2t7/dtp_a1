-- VCS: git@bitbucket.org/schaefers/DT-DEMO-SimpleGates.git
-- PUB: https://pub.informatik.haw-hamburg.de/home/pub/prof/schaefers_michael/*_DT/_CODE_/exampleSimpleGates/...
--
--
-- History:
-- ========
-- 111005 / WS11/12 :
--      1st version for TI3 DT WS11/12  by Michael Schaefers
-- WS14/15 (141104):
--      update for TI3 DT WS14/15  by Michael Schaefers 



library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;       -- required for conv_std_logic_vector()
    use ieee.std_logic_unsigned.all;    -- decision for "std_logic_arith" and hence against "numeric_std_unsigned"





entity sg4gate4 is
    port (
	w : out std_logic;
        x : out std_logic;
        y : out std_logic;
        z : out std_logic
    );--]port
end entity sg4gate4;





architecture beh1 of sg4gate4 is
begin

    sg:
    process is
    begin
        
        w <= '0';  x <= '0';  y <= '0';  z <= '0';   wait for 10 ns;
        w <= '0';  x <= '0';  y <= '0';  z <= '1';   wait for 10 ns;
        w <= '0';  x <= '0';  y <= '1';  z <= '0';   wait for 10 ns;
        w <= '0';  x <= '0';  y <= '1';  z <= '1';   wait for 10 ns;
        w <= '0';  x <= '1';  y <= '0';  z <= '0';   wait for 10 ns;
        w <= '0';  x <= '1';  y <= '0';  z <= '1';   wait for 10 ns;
        w <= '0';  x <= '1';  y <= '1';  z <= '0';   wait for 10 ns;
        w <= '0';  x <= '1';  y <= '1';  z <= '1';   wait for 10 ns;
        w <= '1';  x <= '0';  y <= '0';  z <= '0';   wait for 10 ns;
        w <= '1';  x <= '0';  y <= '0';  z <= '1';   wait for 10 ns;
        w <= '1';  x <= '0';  y <= '1';  z <= '0';   wait for 10 ns;
        w <= '1';  x <= '0';  y <= '1';  z <= '1';   wait for 10 ns;
        w <= '1';  x <= '1';  y <= '0';  z <= '0';   wait for 10 ns;
        w <= '1';  x <= '1';  y <= '0';  z <= '1';   wait for 10 ns;
        w <= '1';  x <= '1';  y <= '1';  z <= '0';   wait for 10 ns;
        w <= '1';  x <= '1';  y <= '1';  z <= '1';   wait for 10 ns;
        
	wait;
        
    end process sg;
    
end architecture beh1;
