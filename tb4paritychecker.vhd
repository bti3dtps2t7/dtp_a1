library work;
    use work.all;                                           -- Zugriff auf Arbeitsbibliothek ("dorthin", wohin alles compiliert wird)

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic
    
    
entity tb4paritychecker is
end entity tb4paritychecker;



architecture beh of tb4paritychecker is
    
    signal i1_s : std_logic;
    signal i2_s : std_logic;
    signal i3_s : std_logic;
    signal i4_s : std_logic;
    signal o_s : std_logic;
    

    component sg4gate4 is
	port(
	     w : out std_logic;
	     x : out std_logic;
	     y : out std_logic;
	     z : out std_logic
	     );
    end component sg4gate4;
    for all : sg4gate4 use entity work.sg4gate4( beh1 );


    component ParityChecker is
	port(
	     o : out std_logic;
	     i1 : in std_logic;
	     i2 : in std_logic;
	     i3 : in std_logic;
	     i4 : in std_logic
	);
    end component ParityChecker;

    for all : ParityChecker use entity work.ParityChecker( rtl);

begin

    sg_i : sg4gate4
	port map(
	   w => i1_s,
	   x => i2_s,
	   y => i3_s,
	   z => i4_s
	);


   ParityChecker_i : ParityChecker
	port map(
	   i1 => i1_s,
	   i2 => i2_s,
	   i3 => i3_s,
	   i4 => i4_s,
	   o => o_s
	);
end architecture beh;

