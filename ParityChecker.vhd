library work;
    use work.all;                                           -- Zugriff auf Arbeitsbibliothek ("dorthin", wohin alles compiliert wird)

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic


entity paritychecker is
    port (
        o  : out std_logic;                                 -- ACHTUNG: "o" ist eigentlich ein schlechter Name -> zu wenig Information im Namen
        i1 : in  std_logic;
        i2 : in  std_logic;
        i3 : in  std_logic;
        i4 : in  std_logic
    );--]port
end entity paritychecker;


architecture rtl of paritychecker is
begin

	checkerlogic:
	process (i1,i2,i3,i4) is
	begin
	o <= (i1 XOR i2) XNOR (i3 XOR i4);
	end process checkerlogic;

end architecture rtl;
